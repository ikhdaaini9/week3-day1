// 1. Buat variable yang isinya array. Namanya bebas kalo bisa sesuaikan konteks. Di dalam array minimal ada 5 data.
// 2. Buat variable yang isinya object. Nama variablenya bebas kalo bisa sesuaikan konteks. Di dalam object minimal ada 4 property. 1 property nilainya harus object lagi <- dalam object tersebut bebas mau di kasi property apa, tapi kalo bisa sesuaikan konteks
// 3. Desctrucing ke 2 variable tersebut. Terus hasil desctructnya tampilin di console ya men-temen
// 4. Buat satu console.log yang nemampilkan data string sama expressi ( variable aja ) pakek template literals ya
// 5. Buat satu variable juga yang dalamnya ada pengkondisian pakek ternary operator. Kemudian hasilnya boleh tampilin lewat console.log
// 6. Buat variable yang isinya array of object. minimal ada 5 data object
// 7. Terus variable array yang no 6. boleh di looping pakek method map dan hasil loopinganya tampilin di console ya guys.
// 8. Masi berhubungan sama variable yang di no 6, silakan buat variable yang isinya hasil dari eksekusi method filter ya guys
// 9. Masi berhubungan sama variable yang di no 6 juga, silakan buat variable yang isinya hasil dari eksekusi method find ya guys.


// Buat di dalam 1 file js aja ya men-temen. Jan lupa di upload di repo gitlab masing-masing. Terus linknya boleh di submit yaww

// no.1
const husbu = ['Dazai', 'Gojo', 'Itachi', 'Todoroki', 'Vanitas'];

// no.2
const anime = {
    title: "BSD",
    genre: "Action",
    score: "100000",
    chara: {
        protag: "Atsushi",
        antag: "Dazai"
    }
}

// no.3 
const [one,two, three] = husbu
console.log(one,two, three)

const {title, genre, chara} = anime
console.log(title, genre)
const {protag} = chara
console.log(protag)

// no. 4
console.log(`this is my fav husbu ${chara.antag} :P`)

// no.5
const coba = (9 > 10 ) ? "Ya" : "Tidak"
console.log(coba)

// no.6
const charaAnime = [
    {
        char: "Dazai",
        role: "antag",
        age: 22,
    },

    {
        char: "Gojo",
        role: "antag",
        age: 32,
    },

    {
        char: "Itachi",
        role: "protag",
        age: 30,
    },

    {
        char: "Todoroki",
        role: "protag",
        age: 16,
    },

    {
        char: "Yato",
        role: "god",
        age: 1000,
    },

];

// no.7
const profileAnime = (charaAnime) => {
    return `This is ${charaAnime.char}, he's ${charaAnime.age} years old and he plays role as ${charaAnime.role}`;
  };
  
  charaAnime.map((mapped) => console.log(profileAnime(mapped)));

// no.8
console.log(charaAnime.filter(filtered => filtered.char.includes("i") && (filtered.role !== "protag" && filtered.role !== "god") && filtered.age > 20));

// no.9
console.log(charaAnime.find(finded => finded.role === "protag" && !finded.char.includes('a')))